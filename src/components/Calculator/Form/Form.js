import Input from "../../UI/Input";
import Select from "../../UI/Select";

const Form = ({typesSelect, inputChangeHandler, dataForm, amountOfCredit}) => {
    // процент первоначального взноса
    const paymentPercent = Math.round((dataForm.firstPayment / dataForm.price) * 100);

    return (
        <div className="calculator">
            <p>Параметры кредитования</p>
            <Select
                id="type"
                label="Вид ипотеки"
                options={typesSelect}
            />
            <Input
                id="cost"
                label="Стоимость квартиры"
                name="price"
                firstValue={dataForm.price}
                type="text"
                setForm={inputChangeHandler}
            />
            <Input
                id="first"
                label="Первый взнос"
                name="firstPayment"
                firstValue={dataForm.firstPayment}
                type="number"
                setPercent={paymentPercent}
                setForm={inputChangeHandler}
            />
            <Input
                id="years"
                label="Срок, лет"
                name="term"
                firstValue={dataForm.term}
                type="number"
                setForm={inputChangeHandler}
            />
            <div className="calculator-footer">
                <div className="calculator-label">Сумма кредита</div>
                <div className="calculator-cost">{amountOfCredit}</div>
            </div>
        </div>
    )
}

export default Form;