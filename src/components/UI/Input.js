import classNames from 'classnames';

const Input = ({ id, label, name, firstValue, type, setPercent, setForm }) => {
    return (
        <div className={classNames('input-block')}>
            {label && <label className="label" htmlFor={id}>
                {label}
            </label>}
            <input 
                id={id} 
                name={name} 
                value={firstValue} 
                type={type} 
                onChange={(e) => {setForm(e.target)}}
            />
            {setPercent !== undefined && <span className="input-percent">{setPercent}%</span>}
        </div>
    )
}

export default Input;