import './App.css';
import Calculator from './components/Calculator/Calculator';

function App() {
  return (
    <div className="App">
      <header>
        Calculator
      </header>
      <main>
        <Calculator />
      </main>
    </div>
  );
}

export default App;
